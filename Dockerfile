FROM azul/zulu-openjdk:11

EXPOSE 80
EXPOSE 9404

ADD prom_jmx_agent.yaml /root/config.yaml
ADD https://repo1.maven.org/maven2/io/prometheus/jmx/jmx_prometheus_javaagent/0.13.0/jmx_prometheus_javaagent-0.13.0.jar /root/prom_jmx_agent.jar

ENTRYPOINT ["java", "-javaagent:/root/prom_jmx_agent.jar=9404:/root/config.yaml", "-jar", "/root/service.jar"]
